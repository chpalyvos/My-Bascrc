# My-Bashrc

It's  my version of .bashrc. 
What is different from common .bashrc files, is that I 've added commands that with which anyone can change the interface of the terminal so that it is not that boring.

Now the workflow is:

1-Download the .bashrc file
2-Navigate to line 400 and remove '#' from the command that expresses your own preference. The interface is described in comments after the command.
The commands that give the interface begin with 'PS1='
3-Save the file
4-$source ~/.bashrc
5-Exit from current terminal
6-Launch a new one and the interface you 've selected will be in usage. Enjoy 
